﻿
using Eich.ORM.SQLite.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eich.ORM.SQLite.Tests.Entities
{
    public class CategoryEntity : ISQLiteEntity
    {
        public Int64 Id { get; set; }
        public String Name { get; set; }
    }
}
