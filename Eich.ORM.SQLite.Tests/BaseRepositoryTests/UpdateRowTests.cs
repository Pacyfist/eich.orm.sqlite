﻿using System;
using System.Collections.Generic;
using Eich.ORM.SQLite.Tests.Entities;
using Eich.ORM.SQLite.Tests.Wrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Eich.ORM.SQLite.Tests.BaseRepositoryTests
{
    [TestClass]
    public class UpdateRowTests
    {
        [TestMethod]
        public void SingleCategory_InsertSelectUpdateSelect_OneRow()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();

            CategoryEntity cat = new CategoryEntity { Id = 1, Name = "Category 1" };

            repo.InsertRow(cat);

            IList<CategoryEntity> actual1 = repo.SelectRows<CategoryEntity>();
            Assert.AreEqual("Category 1", actual1[0].Name);

            cat.Name = "Updated Category 1";

            repo.UpdateRow(cat);

            IList<CategoryEntity> actual2 = repo.SelectRows<CategoryEntity>();
            Assert.AreEqual("Updated Category 1", actual2[0].Name);
        }

        [TestMethod]
        public void SingleCategory_InsertSelectUpdateSelect_ThreeRows()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();

            CategoryEntity cat1 = new CategoryEntity { Id = 1, Name = "Category 1" };
            CategoryEntity cat2 = new CategoryEntity { Id = 2, Name = "Category 2" };
            CategoryEntity cat3 = new CategoryEntity { Id = 3, Name = "Category 3" };

            repo.InsertRow(cat1);
            repo.InsertRow(cat2);
            repo.InsertRow(cat3);

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>((x) => x.Id == 2);
            Assert.AreEqual("Category 2", actual[0].Name);

            cat2.Name = "Updated Category 2";

            repo.UpdateRow(cat2);

            IList<CategoryEntity> actual1 = repo.SelectRows<CategoryEntity>((x) => x.Id == 1);
            Assert.AreEqual("Category 1", actual1[0].Name);

            IList<CategoryEntity> actual2 = repo.SelectRows<CategoryEntity>((x) => x.Id == 2);
            Assert.AreEqual("Updated Category 2", actual2[0].Name);

            IList<CategoryEntity> actual3 = repo.SelectRows<CategoryEntity>((x) => x.Id == 3);
            Assert.AreEqual("Category 3", actual3[0].Name);
        }
    }
}
