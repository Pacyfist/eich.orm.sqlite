﻿using System;
using Eich.ORM.SQLite.Tests.Entities;
using Eich.ORM.SQLite.Tests.Wrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Eich.ORM.SQLite.Tests.BaseRepositoryTests
{
    [TestClass]
    public class ExistsTableTests
    {
        [TestMethod]
        public void CheckForTable_EmptyFile()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            bool actual = repo.ExistsTable<CategoryEntity>();

            Assert.AreEqual(false, actual);
        }

        [TestMethod]
        public void CheckForTable__AfterTableCreation()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            bool actual;

            repo.CreateTable<CategoryEntity>();

            actual = repo.ExistsTable<CategoryEntity>();

            Assert.AreEqual(true, actual);

            repo.DropTable<CategoryEntity>();

            actual = repo.ExistsTable<CategoryEntity>();

            Assert.AreEqual(false, actual);
        }

        [TestMethod]
        public void CheckForTable__AfterTableTableDrop()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            bool actual;

            repo.CreateTable<CategoryEntity>();
            repo.DropTable<CategoryEntity>();

            actual = repo.ExistsTable<CategoryEntity>();

            Assert.AreEqual(false, actual);
        }
    }
}
