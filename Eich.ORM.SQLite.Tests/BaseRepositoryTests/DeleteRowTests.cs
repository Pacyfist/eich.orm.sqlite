﻿using Eich.ORM.SQLite.Repositories;
using Eich.ORM.SQLite.Tests.Entities;
using Eich.ORM.SQLite.Tests.Wrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Eich.ORM.SQLite.Tests.BaseRepositoryTests
{
    [TestClass]
    public class DeleteRowTests
    {
        [TestMethod]
        public void SingleCategory_InsertOneDeleteAll_ReturnsZero()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });

            repo.DeleteRows<CategoryEntity>();

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>();

            Assert.AreEqual(0, actual.Count);
        }

        public void SingleCategory_InsertTwoDeleteAll_ReturnsZero()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });
            repo.InsertRow(new CategoryEntity { Id = 2, Name = "Category 2" });

            repo.DeleteRows<CategoryEntity>();

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>();

            Assert.AreEqual(0, actual.Count);
        }

        public void SingleCategory_InsertTwoDeleteOne_ReturnsOne()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });
            repo.InsertRow(new CategoryEntity { Id = 2, Name = "Category 2" });

            repo.DeleteRows<CategoryEntity>((x) => x.Id == 1);

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>();

            Assert.AreEqual(1, actual.Count);
        }

        public void SingleCategory_InsertTwoDeleteFirst_ReturnsSecond()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });
            repo.InsertRow(new CategoryEntity { Id = 2, Name = "Category 2" });

            repo.DeleteRows<CategoryEntity>((x) => x.Id == 1);

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>();

            Assert.AreEqual("Category 2", actual[0].Name);
        }

        public void SingleCategory_InsertTwoDeleteSecond_ReturnsFirst()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });
            repo.InsertRow(new CategoryEntity { Id = 2, Name = "Category 2" });

            repo.DeleteRows<CategoryEntity>((x) => x.Id == 1);

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>();

            Assert.AreEqual("Category 2", actual[0].Name);
        }

        public void SingleCategory_InsertDeleteInsert_ReturnsOne()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });
            repo.DeleteRows<CategoryEntity>((x) => x.Id == 1);
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>();

            Assert.AreEqual(1, actual.Count);
        }
    }
}
