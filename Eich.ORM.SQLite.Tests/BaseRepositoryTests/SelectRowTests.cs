using Eich.ORM.SQLite.Repositories;
using Eich.ORM.SQLite.Tests.Entities;
using Eich.ORM.SQLite.Tests.Wrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Eich.ORM.SQLite.Tests.BaseRepositoryTests
{
    [TestClass]
    public class SelectRowTests
    {
        [TestMethod]
        public void SingleCategory_InsertSelect_ReturnsOne()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>();

            Assert.AreEqual(1, actual.Count);
        }

        [TestMethod]
        public void TwoCategories_InsertSelect_ReturnsTwo()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });
            repo.InsertRow(new CategoryEntity { Id = 2, Name = "Category 2" });

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>();

            Assert.AreEqual(2, actual.Count);
        }

        [TestMethod]
        public void TwoCategories_InsertSelect_FromTwo_WhereIdIs1()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });
            repo.InsertRow(new CategoryEntity { Id = 2, Name = "Category 2" });

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>((x) => x.Id == 1);

            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(1, actual[0].Id);
            Assert.AreEqual("Category 1", actual[0].Name);
        }

        [TestMethod]
        public void TwoCategories_InsertSelect_FromTwo_WhereIdIs2()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });
            repo.InsertRow(new CategoryEntity { Id = 2, Name = "Category 2" });

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>((x) => x.Id == 2);

            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(2, actual[0].Id);
            Assert.AreEqual("Category 2", actual[0].Name);
        }

        [TestMethod]
        public void TwoCategories_InsertSelect_FromTwo_WhereNameIsCategory1()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });
            repo.InsertRow(new CategoryEntity { Id = 2, Name = "Category 2" });

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>((x) => x.Name == "Category 1");

            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(1, actual[0].Id);
            Assert.AreEqual("Category 1", actual[0].Name);
        }

        [TestMethod]
        public void TwoCategories_InsertSelect_FromTwo_WhereNameIsCategory2()
        {
            BaseRepositoryWrapper repo = new BaseRepositoryWrapper();

            repo.CreateTable<CategoryEntity>();
            repo.InsertRow(new CategoryEntity { Id = 1, Name = "Category 1" });
            repo.InsertRow(new CategoryEntity { Id = 2, Name = "Category 2" });

            IList<CategoryEntity> actual = repo.SelectRows<CategoryEntity>((x) => x.Name == "Category 2");

            Assert.AreEqual(1, actual.Count);
            Assert.AreEqual(2, actual[0].Id);
            Assert.AreEqual("Category 2", actual[0].Name);
        }

    }
}
