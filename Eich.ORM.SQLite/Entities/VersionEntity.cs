﻿using Eich.ORM.SQLite.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eich.ORM.SQLite.Entities
{
    public class VersionEntity : ISQLiteEntity
    {
        public Int64 Id { get; set; }
        public String Version { get; set; }
    }
}
