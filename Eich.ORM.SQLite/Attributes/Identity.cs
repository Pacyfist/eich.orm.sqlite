﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eich.ORM.SQLite.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class Identity : Attribute
    {
    }
}
