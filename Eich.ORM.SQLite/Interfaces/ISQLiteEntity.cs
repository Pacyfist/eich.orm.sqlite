﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eich.ORM.SQLite.Interfaces
{
    public interface ISQLiteEntity
    {
        Int64 Id { get; set; }
    }
}
