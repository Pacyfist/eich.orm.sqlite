﻿using Eich.ORM.SQLite.Interfaces;
using NLog;
using System;
using System.IO;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Eich.ORM.SQLite.Repositories
{
    public abstract class BaseRepository
    {
        protected Logger _log;
        protected string _file;

        #region Constructor
        public BaseRepository()
        {
            this._log = null;
            this._file = Path.GetTempFileName();
        }
        #endregion

        #region Public Methods
        public bool ExistsTable<T>() where T : ISQLiteEntity
        {
            String query = GenerateExistsTableQuery<T>();

            Boolean exists = ExecuteQueryWithResult<Boolean>(query);

            return exists;
        }

        public void CreateTable<T>() where T : ISQLiteEntity
        {
            String query = GenerateCreateTableQuery<T>();

            ExecuteQuery(query);
        }

        public void DropTable<T>() where T : ISQLiteEntity
        {
            String query = GenerateDropTableQuery<T>();

            ExecuteQuery(query);
        }

        public void InsertRow<T>(T row) where T : ISQLiteEntity
        {
            String query = GenerateInsertRowQuery<T>(row);

            ExecuteQuery(query);
        }

        public void UpdateRow<T>(T row) where T : ISQLiteEntity
        {
            String query = GenerateUpdateRowQuery<T>(row);

            ExecuteQuery(query);
        }

        public void DeleteRows<T>(Expression<Func<T, bool>> where = null) where T : ISQLiteEntity
        {
            String query = GenerateDeleteRowsQuery<T>(where);

            ExecuteQuery(query);
        }

        public IList<T> SelectRows<T>(Expression<Func<T, bool>> where = null) where T : ISQLiteEntity, new()
        {
            String query = GenerateSelectRowsQuery<T>(where);

            return ExecuteQueryWithResults<T>(query);
        }

        public Int64 CountRows<T>(Expression<Func<T, bool>> where = null) where T : ISQLiteEntity
        {
            String query = GenerateCountRowsQuery<T>(where);

            return ExecuteQueryWithResult<Int64>(query);
        }
        #endregion

        #region Protected Methods
        protected String GenerateExistsTableQuery<T>() where T : ISQLiteEntity
        {
            return String.Format("SELECT true FROM sqlite_master WHERE type='table' AND name='{0}';", typeof(T).Name);
        }

        protected String GenerateCreateTableQuery<T>() where T : ISQLiteEntity
        {
            Type type = typeof(T);

            IDictionary<String, String> columns = new Dictionary<String, String>();

            foreach (var p in type.GetProperties())
            {
                String sqlType = MapCSTypeToSQLType(p.PropertyType);

                if (!String.IsNullOrEmpty(sqlType))
                {
                    columns[p.Name] = sqlType;
                }
            }

            return String.Format("CREATE TABLE {0} ({1})", type.Name, String.Join(", ", columns.Select(c => String.Format("{0} {1}", c.Key, c.Value))));
        }

        protected String GenerateDropTableQuery<T>() where T : ISQLiteEntity
        {
            Type type = typeof(T);

            return String.Format("DROP TABLE IF EXISTS {0}", type.Name);
        }

        protected String GenerateInsertRowQuery<T>(T row) where T : ISQLiteEntity
        {
            Type type = typeof(T);

            IList<String> names = new List<String>();
            IList<String> values = new List<String>();

            foreach (var p in type.GetProperties())
            {
                String sqlType = MapCSTypeToSQLType(p.PropertyType);

                if (!String.IsNullOrEmpty(sqlType))
                {
                    names.Add(p.Name);

                    if (p.PropertyType == typeof(Int64) && p.Name == "Id" && (Int64)p.GetValue(row) == 0)
                    {
                        values.Add(String.Format("{0}", 1 + ExecuteQueryWithResult<Int64>(String.Format("SELECT Id FROM {0} ORDER BY id DESC LIMIT 1", type.Name))));
                    }
                    else
                    {
                        object value = p.GetValue(row);

                        if (value == null)
                        {
                            values.Add("NULL");
                        }
                        else
                        {
                            if (p.PropertyType.IsValueType && !p.PropertyType.IsGenericType)
                            {
                                values.Add(String.Format("{0}", p.GetValue(row)));
                            }
                            else
                            {
                                values.Add(String.Format("\"{0}\"", value.ToString().Replace("\"", "\"\"")));
                            }
                        }
                    }
                }
            }

            return String.Format("INSERT INTO {0} ({1}) VALUES ({2})", type.Name, String.Join(", ", names), String.Join(", ", values));
        }

        protected String GenerateUpdateRowQuery<T>(T row) where T : ISQLiteEntity
        {
            Type type = typeof(T);

            IDictionary<String, String> columns = new Dictionary<String, String>();

            foreach (var p in type.GetProperties())
            {
                String sqlType = MapCSTypeToSQLType(p.PropertyType);

                if (!String.IsNullOrEmpty(sqlType))
                {
                    String name = p.Name;
                    object value = p.GetValue(row);

                    if (value == null)
                    {
                        columns[name] = "NULL";
                    }
                    else
                    {
                        if (p.PropertyType.IsValueType && !p.PropertyType.IsGenericType)
                        {
                            columns[name] = String.Format("{0}", p.GetValue(row));
                        }
                        else
                        {
                            columns[name] = String.Format("\"{0}\"", value.ToString().Replace("\"", "\"\""));
                        }
                    }
                }
            }

            return String.Format("UPDATE {0} SET {1} Where Id={2}", type.Name, String.Join(", ", columns.Where(s => s.Key != "Id").Select(c => String.Format("{0}={1}", c.Key, c.Value))), columns["Id"]);
        }

        protected String GenerateDeleteRowsQuery<T>(Expression<Func<T, bool>> where) where T : ISQLiteEntity
        {
            Type type = typeof(T);

            if (where == null)
                return String.Format("DELETE FROM {0}", type.Name);
            else
                return String.Format("DELETE FROM {0} WHERE {1}", type.Name, ExpressionToQuery(where.Body));
        }

        protected String GenerateSelectRowsQuery<T>(Expression<Func<T, bool>> where) where T : ISQLiteEntity
        {
            Type type = typeof(T);

            if (where == null)
                return String.Format("SELECT * FROM {0}", type.Name);
            else
                return String.Format("SELECT * FROM {0} WHERE {1}", type.Name, ExpressionToQuery(where.Body));
        }

        protected String GenerateCountRowsQuery<T>(Expression<Func<T, bool>> where) where T : ISQLiteEntity
        {
            Type type = typeof(T);

            if (where == null)
                return String.Format("SELECT COUNT(*) FROM {0}", type.Name);
            else
                return String.Format("SELECT COUNT(*) FROM {0}", type.Name, ExpressionToQuery(where.Body));
        }

        protected String ExpressionToQuery(Expression expression)
        {
            if (expression is BinaryExpression)
            {
                BinaryExpression be = expression as BinaryExpression;

                String left = ExpressionToQuery(be.Left);
                String right = ExpressionToQuery(be.Right);

                String result = "";

                switch (be.NodeType)
                {
                    case ExpressionType.OrElse:
                        result = String.Format("({0} OR {1})", left, right);
                        break;
                    case ExpressionType.AndAlso:
                        result = String.Format("({0} AND {1})", left, right);
                        break;
                    case ExpressionType.Equal:
                        result = String.Format("{0} = {1}", left, right);
                        break;
                    case ExpressionType.NotEqual:
                        result = String.Format("{0} <> {1}", left, right);
                        break;
                    default:
                        break;
                }

                return result;
            }

            if (expression is MemberExpression)
            {
                MemberExpression me = expression as MemberExpression;
                return me.Member.Name;
            }

            if (expression is ConstantExpression)
            {
                ConstantExpression ce = expression as ConstantExpression;

                object value = ce.Value;

                if (value == null)
                    return "NULL";

                if (value is String)
                    return String.Format("'{0}'", value);
                else
                    return String.Format("{0}", value);
            }

            return "___";
        }

        protected String MapCSTypeToSQLType(Type type)
        {
            if (type == (typeof(Int64)))
                return "INTEGER NOT NULL";

            if (type == typeof(Int64?))
                return "INTEGER";

            if (type == typeof(String))
                return "TEXT";

            if (type == typeof(DateTime))
                return "DATETIME NOT NULL";

            if (type == typeof(DateTime?))
                return "DATETIME";

            throw new Exception(String.Format("Unmapped CSType: {0}", type.Name));

        }

        protected void ExecuteQuery(String query)
        {
            try
            {
                if (_log != null)
                {
                    _log.Debug(String.Format("ExecuteQuery | {0}", query));
                }

                using (SQLiteConnection connection = new SQLiteConnection(String.Format("Data Source={0};Version=3;", _file)))
                {
                    connection.Open();

                    SQLiteCommand cmd = new SQLiteCommand(query, connection);

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                if (_log != null)
                {
                    _log.Error(ex);
                }
                else
                {
                    throw;
                }
            }

        }

        protected IList<T> ExecuteQueryWithResults<T>(String query) where T : ISQLiteEntity, new()
        {
            List<T> results = new List<T>();

            try
            {
                if (_log != null)
                {
                    _log.Debug(String.Format("ExecuteQueryWithResults | {0}", query));
                }

                using (SQLiteConnection connection = new SQLiteConnection(String.Format("Data Source={0};Version=3;", _file)))
                {
                    connection.Open();

                    SQLiteCommand cmd = new SQLiteCommand(query, connection);

                    SQLiteDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            T result = new T();

                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (!reader.IsDBNull(i))
                                {
                                    var name = reader.GetName(i);
                                    var value = reader.GetValue(i);

                                    typeof(T).GetProperty(name).SetValue(result, value);
                                }
                            }

                            results.Add(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (_log != null)
                {
                    _log.Error(ex);
                }
                else
                {
                    throw;
                }
            }

            return results;
        }

        protected T ExecuteQueryWithResult<T>(String query)
        {
            T result;

            using (SQLiteConnection connection = new SQLiteConnection(String.Format("Data Source={0};Version=3;", _file)))
            {
                connection.Open();

                object scalar = new SQLiteCommand(query, connection).ExecuteScalar();

                if (scalar != null)
                {
                    result = (T)Convert.ChangeType(scalar, typeof(T));
                }
                else
                {
                    result = default(T);
                }
            }

            return result;
        }
        #endregion

    }
}
