﻿using Eich.ORM.SQLite.Entities;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Eich.ORM.SQLite.Repositories
{
    public class SettingsRepository : BaseRepository
    {
        #region Singleton
        private static SettingsRepository _instance = null;

        public static SettingsRepository GetInstance(string name)
        {
            if (_instance == null)
                _instance = new SettingsRepository(name);

            return _instance;
        }
        #endregion

        #region Constructor
        private SettingsRepository(string name)
        {
            this._log = LogManager.GetCurrentClassLogger();
            this._file = GetFilePath(name);

            this.CreateTable<VersionEntity>();
            this.InsertRow<VersionEntity>(new VersionEntity() { Version = "0.1" });
        }
        #endregion

        private string GetFilePath(string folderName)
        {
            string appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string filename = @"settings";

            string folderPath = Path.Combine(appdata, folderName);
            string filePath = Path.Combine(appdata, folderName, filename);

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            if (!File.Exists(filePath))
                File.Create(filePath);

            return filePath;
        }

    }
}
